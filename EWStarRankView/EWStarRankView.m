/*
 
 Copyright 2012 Tzu-Yi Lin
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
*/

#import "EWStarRankView.h"

@implementation EWStarRankView

@synthesize starNumber = _starNumber,
            marginSize = _marginSize,
            isSupportHalfStar = _supportHalfStar,
            editable = _editable,
            rank = _rank,
            starImage = _starImage,
            starHighlightImage = _starHighlightImage,
            position = _position,
            delegate = _delegate;

- (id)initWithStarNumber:(NSUInteger)number supportHalfStar:(BOOL)half
{
    self = [super initWithFrame:CGRectMake(0, 0, 0, 0)];
    if (self) {
        _starNumber = number;
        _supportHalfStar = half;
        _editable = YES;
    }
    
    return self;
}

- (void)dealloc
{
    self.starImage = nil;
    self.starHighlightImage = nil;
    [super dealloc];
}

- (NSUInteger)starForPoint:(CGPoint)point
{
    NSUInteger i;
    for (i = 0; i < _starNumber; i++) {
        UIView *image = [self viewWithTag:i + 1000];
        if (CGRectContainsPoint(image.frame, point))
            break;
    }
    
    return (i < _starNumber) ? i + 1: -1;
}

- (CGPoint)pointForStarPosition:(NSInteger)position
{
    CGSize imageSize = [_starImage size];
    CGFloat x = imageSize.width * position + _marginSize * (position + 1);
    CGFloat y = _marginSize;
    
    return CGPointMake(x, y);
}

- (void)updateFrame
{
    CGSize starSize = [_starImage size];
    CGSize frameSize = CGSizeMake(starSize.width * _starNumber + _marginSize * (_starNumber + 1), 
                                  starSize.height + _marginSize * 2);
    
    self.frame = CGRectMake(_position.x, _position.y, frameSize.width, frameSize.height);
    [self setNeedsDisplay];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize starSize = [_starImage size];
    for (UIView *view in self.subviews)
        [view removeFromSuperview];
    
    for (NSUInteger i = 0; i < _starNumber; i++) {
        CGPoint imagePoint = [self pointForStarPosition:i];
        UIImageView *starImage = [[UIImageView alloc] initWithFrame:CGRectMake(imagePoint.x, imagePoint.y,
                                                                               starSize.width, starSize.height)];
        starImage.image = (i + 1 <= _rank) ? _starHighlightImage : _starImage;
        starImage.tag = i + 1000;
        [self addSubview:starImage];
        [starImage release];
    }
}

// setter and getter
- (void)setRank:(float)rank
{
    _rank = rank;
    [self setNeedsDisplay];
}

- (void)setStarImage:(UIImage *)starImage
{
    [_starImage release];
    _starImage = [starImage retain];
    [self updateFrame];
}

- (void)setStarHighlightImage:(UIImage *)starHighlightImage
{
    [_starHighlightImage release];
    _starHighlightImage = [starHighlightImage retain];
    [self updateFrame];
}

- (void)setPosition:(CGPoint)position
{
    _position = position;
    [self updateFrame];
}

- (void)setMarginSize:(CGFloat)marginSize
{
    _marginSize = marginSize;
    [self updateFrame];
}

// user interaction
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_editable)
        return;
    
    CGPoint touchPoint = [[touches anyObject] locationInView:self];
    NSUInteger testTouch = [self starForPoint:touchPoint];
    
    if (testTouch != -1) {
        _rank = testTouch;
        [self setNeedsLayout];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_editable)
        return;
    
    CGPoint touchPoint = [[touches anyObject] locationInView:self];
    NSUInteger testTouch = [self starForPoint:touchPoint];
    
    if (testTouch != -1) {
        _rank = testTouch;
        [self setNeedsLayout];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (!_editable)
        return;
    
    if ([_delegate respondsToSelector:@selector(starRankDidChange:rank:)])
        [_delegate starRankDidChange:self rank:_rank];
}

@end

/*
 
 Copyright 2012 Tzu-Yi Lin
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 */

#import <UIKit/UIKit.h>

@protocol EWStarRankDelegate;

@interface EWStarRankView : UIControl {
    NSUInteger _starNumber;
    CGFloat _marginSize;
    CGPoint _position;
    float _rank;
    BOOL _supportHalfStar;
    BOOL _editable;
    
    UIImage *_starImage;
    UIImage *_starHighlightImage;
    id <EWStarRankDelegate> _delegate;
}

@property (nonatomic, assign) NSUInteger starNumber;
@property (nonatomic, assign) CGFloat marginSize;
@property (nonatomic, readonly) BOOL isSupportHalfStar;
@property (nonatomic, assign) float rank;
@property (nonatomic, assign) BOOL editable;

@property (nonatomic, retain) UIImage *starImage;
@property (nonatomic, retain) UIImage *starHighlightImage;
@property (nonatomic, assign) id <EWStarRankDelegate> delegate;
@property (nonatomic, assign) CGPoint position;

// half star mode not support yet
- (id)initWithStarNumber:(NSUInteger)number supportHalfStar:(BOOL)half;

@end


@protocol EWStarRankDelegate <NSObject>
@optional

- (void)starRankDidChange:(EWStarRankView *)rankView rank:(float)rank;

@end

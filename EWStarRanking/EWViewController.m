/*
 
 Copyright 2012 Tzu-Yi Lin
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 
 */

#import "EWViewController.h"

@interface EWViewController ()

@end

@implementation EWViewController

- (void)dealloc
{
    [_starRankView release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    _starRankView = [[EWStarRankView alloc] initWithStarNumber:5 supportHalfStar:NO];
    _starRankView.starImage = [UIImage imageNamed:@"star.png"];
    _starRankView.starHighlightImage = [UIImage imageNamed:@"starhighlighted.png"];
    _starRankView.marginSize = 5;
    _starRankView.rank = 0;
    _starRankView.delegate = self;
    
    [self.view addSubview:_starRankView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)starRankDidChange:(EWStarRankView *)rankView rank:(float)rank
{
    NSLog(@"Rank End: %f", rank);
}

@end
